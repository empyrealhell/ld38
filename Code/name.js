var Name = {
	_onsets: [ "p", "t", "k", "ch", "f", "ph", "th", "s", "sh", "h", "b", "d", "g", "j", "v", "z", "m", "n", "y", "w", "r", "l", "pl", "bl", "cl", "gl", "pr", "br", "tr", "dr", "cr", "gr", "tw", "dw", "qu", "gu", "pu", "fl", "sl", "fr", "thr", "shr", "sw", "thw", "sp", "st", "sk", "sm", "sn", "spl", "spr", "str", "scr", "squ" ],
	
	_vowels: ["a", "o", "ough", "i", "ee", "e", "u", "oo", "oi", "oa", "ou", "ur", "ar", "or", "ear", "are", "ure", "er", "y"],
	
	_codas: [ "p", "t", "k", "ch", "f", "th", "s", "sh", "b", "d", "g", "j", "v", "z", "m", "n", "ng", "r", "l", "lp", "lb", "lt", "ld", "lch", "lge", "lk", "rp", "rb", "rt", "rd", "rch", "rge", "rk", "rgue", "lf", "lve", "lth", "lse", "lsh", "rf", "rve", "rth", "rce", "rs", "rsh", "lm", "ln", "rm", "rn", "rl", "mp", "nt", "nd", "nch", "nge", "nk", "mph", "mth", "nth", "nce", "nze", "ngth", "ft", "sp", "st", "sk", "fth", "pt", "ct", "pth", "pse", "tz", "dth", "x", "cks", "lpt", "lps", "lfth", "ltz", "lst", "lx", "lcks", "rmth", "rpt", "rpse", "rtz", "rst", "rct", "mpt", "mpse", "ndth", "nct", "nx", "nks", "ngth", "xth", "xt" ],
	
	Generate: function() {
		var syllableCount = 1;//Math.floor(Math.random() * 4) + 1;
		var word = "";
		var lastStructure = 0;
		for (var i = 0; i < syllableCount; i++) {
			var syllableStructure = Math.floor(Math.random() * 2) + 1;
			if (syllableStructure & 1 == 1)
				word += Utils.RandomElement(this._onsets);
			word += this._vowels[Math.floor(Math.random() * this._vowels.length)];
			if (syllableStructure & 2 == 2)
				word += Utils.RandomElement(this._codas);
			lastStructure = syllableStructure;
		}
		return word;
	},
};