var Scene = {
	Initialize: function() {
	},
	
	_currentScene: null,
	_lastFrame: 0,
	
	SwitchTo: function(newScene) {
		if (this._currentScene !== null && this._currentScene.Close !== undefined) {
			this._currentScene.Close();
			window.clearInterval();
		}
		this._currentScene = newScene;
		if (this._currentScene.Open !== undefined)
			this._currentScene.Open();
		Scene._lastFrame = new Date().getTime();
		window.setInterval(function() {
				var currentTime = new Date().getTime();
				var delta = (currentTime - Scene._lastFrame) / 1000;
				Scene._lastFrame = currentTime;
				Scene._currentScene.Run(delta);
			}, 16);
	},
};

var Scenes = {
	Start: {
		_save: null,
		
		Open: function() {
			var saveData = null;
			try { saveData = JSON.parse(Data.Get("saveData")); } catch (e) { }
			if (saveData === null) {
				saveData = {
					Level: 0,
					Funds: 0,
					Debris: 0,
					Upgrades: [],
				};
				Data.Set("saveData", JSON.stringify(saveData));
			}
			this._save = saveData;
		},
		Run: function(delta) {
			Render.GradientClear(0, 1, 1, 0, "#204040", "#306060");
			Render.TextFade(Render._canvas.width / 2, 200, "Yonder Heap", 175, "#80C0C0", "#408080", 1);
			Render.TextFade(Render._canvas.width / 2, 300, "Your world is being flooded with garbage, collect and ", 28, "#80C0C0", "#408080", 1);
			Render.TextFade(Render._canvas.width / 2, 350, "recycle it before the whole world is covered in trash.", 28, "#80C0C0", "#408080", 1);
			if (this._save.Level > 0) {
				var left = Render._canvas.width / 2 - 300;
				var top = 450;
				var width = 250;
				var height = 50;
				if (Utils.IsMouseOver(left, top, width, height)) {
					Render.Button(left, top, width, height, "Continue", "#408080", "#A0C0C0", "#A0C0C0");
					if (Input.Mouse.Clicked) {
						Scene.SwitchTo(Scenes.Upgrade);
					}
				} else {
					Render.Button(left, top, width, height, "Continue", "#306060", "none", "#60A0A0");
				}
				left = Render._canvas.width / 2 + 50
				if (Utils.IsMouseOver(left, top, width, height)) {
					Render.Button(left, top, width, height, "New Game", "#408080", "#A0C0C0", "#A0C0C0");
					if (Input.Mouse.Clicked) {
						Data.Remove("saveData");
						Scene.SwitchTo(Scenes.Upgrade);
					}
				} else {
					Render.Button(left, top, width, height, "New Game", "#306060", "none", "#60A0A0");
				}
			} else {
				var left = Render._canvas.width / 2 - 125;
				var top = 450;
				var width = 250;
				var height = 50;
				if (Utils.IsMouseOver(left, top, width, height)) {
					Render.Button(left, top, width, height, "New Game", "#408080", "#A0C0C0", "#A0C0C0");
					if (Input.Mouse.Clicked) {
						Data.Remove("saveData");
						Scene.SwitchTo(Scenes.Upgrade);
					}
				} else {
					Render.Button(left, top, width, height, "New Game", "#306060", "none", "#60A0A0");
				}
			}
		},
		Close: function() {
			
		}
	},
	Defeat: {
		_messages: [],
		
		Open: function() {
			this._messages.length = 0;

			this._messages.push({ Text: "You've reached critical mass", Y: 150, Start: 0, FadeTime: 1, Duration: 13});
			this._messages.push({ Text: "The entire world is covered in garbage", Y: 250, Start: 2, FadeTime: 1, Duration: 11});
			this._messages.push({ Text: "You sadly make your way back to base", Y: 350, Start: 4, FadeTime: 1, Duration: 9});
			this._messages.push({ Text: "To live out the rest of your life on top of the heap", Y: 450, Start: 6, FadeTime: 1, Duration: 7});
			
			this._messages.push({ Text: "Gross...", Y: 300, Start: 13, FadeTime: 1, Duration: 18});
			this._elapsed = 0;
		},
		_elapsed: 0,
		Run: function(delta) {
			this._elapsed += delta;
			Render.Clear();
			Render.TrashLine(Render._canvas.width, 20, 20, "#000000", "#404040");
			for (var i = 0; i < this._messages.length; i++) {
				var current = this._messages[i];
				if (current.Start <= this._elapsed) {
					if (current.Started === undefined) {
						current.Started = this._elapsed;
					}
					var currentElapsed = this._elapsed - current.Started;
					if (currentElapsed < current.FadeTime) {
						Render.TextFade(Render._canvas.width / 2, current.Y, current.Text, 48, "#80C0C0", "#408080", Math.log10(1 + currentElapsed / current.FadeTime * 9));
					} else if (currentElapsed < current.Duration - current.FadeTime) {
						Render.TextFade(Render._canvas.width / 2, current.Y, current.Text, 48, "#80C0C0", "#408080", 1);
					} else if (currentElapsed < current.Duration) {
						currentElapsed -= (current.Duration - current.FadeTime);
						Render.TextFade(Render._canvas.width / 2, current.Y, current.Text, 48, "#80C0C0", "#408080", Math.log10(1 + (1 - currentElapsed / current.FadeTime) * 9));
					} else {
						this._messages.splice(i, 1);
					}
				}
			}
			
			if (this._messages.length <= 0) {
				Scene.SwitchTo(Scenes.Start);
			}
		},
		Close: function() {
			
		}
	},
	Victory: {
		_messages: [],
		_save: null,
		
		Open: function() {
			var saveData = null;
			try { saveData = JSON.parse(Data.Get("saveData")); } catch (e) { }
			if (saveData === null) {
				saveData = {
					Level: 0,
					Funds: 0,
					Debris: 0,
					Upgrades: [],
				};
				Data.Set("saveData", JSON.stringify(saveData));
			}
			this._save = saveData;
			this._messages.length = 0;
			this._messages.push({ Text: "That was the last of it!", Y: 150, Start: 0, FadeTime: 1, Duration: 10});
			this._messages.push({ Text: "The onslaught of garbage has subsided", Y: 250, Start: 1, FadeTime: 1, Duration: 9});
			this._messages.push({ Text: "You return to base", Y: 350, Start: 2, FadeTime: 1, Duration: 8});
			this._messages.push({ Text: "To continue your recycling, patchouli-smoking life", Y: 450, Start: 3, FadeTime: 1, Duration: 7});
			
			this._messages.push({ Text: "THAAAAAAAANKS!", Y: 300, Start: 10, FadeTime: 1, Duration: 18});
			this._elapsed = 0;
		},
		_elapsed: 0,
		Run: function(delta) {
			this._elapsed += delta;
			Render.Clear();
			Render.TrashLine(Render._canvas.width, 20, 20, "#000000", "#404040");
			for (var i = 0; i < this._messages.length; i++) {
				var current = this._messages[i];
				if (current.Start <= this._elapsed) {
					if (current.Started === undefined) {
						current.Started = this._elapsed;
					}
					var currentElapsed = this._elapsed - current.Started;
					if (currentElapsed < current.FadeTime) {
						Render.TextFade(Render._canvas.width / 2, current.Y, current.Text, 48, "#80C0C0", "#408080", Math.log10(1 + currentElapsed / current.FadeTime * 9));
					} else if (currentElapsed < current.Duration - current.FadeTime) {
						Render.TextFade(Render._canvas.width / 2, current.Y, current.Text, 48, "#80C0C0", "#408080", 1);
					} else if (currentElapsed < current.Duration) {
						currentElapsed -= (current.Duration - current.FadeTime);
						Render.TextFade(Render._canvas.width / 2, current.Y, current.Text, 48, "#80C0C0", "#408080", Math.log10(1 + (1 - currentElapsed / current.FadeTime) * 9));
					} else {
						this._messages.splice(i, 1);
					}
				}
			}
			
			if (this._messages.length <= 0) {
				Scene.SwitchTo(Scenes.Start);
			}
		},
		Close: function() {
			
		}
	},
	Upgrade: {
		_buttons: [],
		_currentPage: 0,
		_save: null,
		
		Open: function() {
			var saveData = null;
			try { saveData = JSON.parse(Data.Get("saveData")); } catch (e) { }
			if (saveData === null) {
				saveData = {
					Level: 0,
					Funds: 0,
					Debris: 0,
					Upgrades: [],
				};
				Data.Set("saveData", JSON.stringify(saveData));
			}
			this._save = saveData;
		},
		Run: function(delta) {
			Render.GradientClear(0, 1, 1, 0, "#204040", "#306060");
			for (var i = 0; i < 13; i++)
				Render.Line(Render._canvas.width - (300 - i * 8), (i + 1) * 8, Render._canvas.width, (i + 1) * 8, "#408080");
			Render.CurrencyText(Render._canvas.width - 10, 120 + 12, "¤" + Math.floor(this._save.Funds), "#80C0C0", 24);
			Render.Title(10, 134, "UPGRADES", "#80C0C0");
			Render.Line(0, 144, Render._canvas.width, 144, "#408080");
			Render.Line(220, 144, 220, Render._canvas.height, "#408080");
			var hover = null;
			for (var i = 0; i < UpgradeCategories.length; i++) {
				var left = 10;
				var width = 200;
				var top = 154 + i * 58;
				var height = 48;
				if (Utils.IsMouseOver(left, top, width, height)) {
					if (this._currentPage == i) {
						Render.Button(left, top, width, height, UpgradeCategories[i].Name, "none", "none", "#80C0C0");
					} else {
						Render.Button(left, top, width, height, UpgradeCategories[i].Name, "#306060", "none", "#60A0A0");
						if (Input.Mouse.Clicked) {
							//yay for logic in the ui...
							this._currentPage = i;
						}
					}
				} else {
					if (this._currentPage == i) {
						Render.Button(left, top, width, height, UpgradeCategories[i].Name, "none", "none", "#80C0C0");
					} else {
						Render.Button(left, top, width, height, UpgradeCategories[i].Name, "none", "none", "#60A0A0");
					}
				}
			}
			var left = 10;
			var width = 200;
			var top = Render._canvas.height - 58;
			var height = 48;
			if (Utils.IsMouseOver(left, top, width, height)) {
				Render.Button(left, top, width, height, "Exit", "#306060", "none", "#60A0A0");
				if (Input.Mouse.Clicked) {
					Scene.SwitchTo(Scenes.Game);
				}
			} else {
				Render.Button(left, top, width, height, "Exit", "none", "none", "#60A0A0");
			}
			var contents = UpgradeCategories[this._currentPage].Contents;
			var gridPosition = [];
			var columns = 0;
			for (var i = 0; i < contents.length; i++) {
				var row = -1;
				var column = -1;
				if (contents[i].Requires === undefined) {
					row = 0;
					column = columns++;
				} else {
					row = 1;
					var current = contents[i].Requires;
					while (current.Requires !== undefined) {
						row++;
						current = current.Requires;
					}
					var index = contents.indexOf(contents[i].Requires);
					column = gridPosition[index].column;
				}
				gridPosition[i] = {
					row: row,
					column: column
				};
			}
			for (var i = 0; i < contents.length; i++) {
				left = 230 + 245 * gridPosition[i].column;
				top = 154 + gridPosition[i].row * 150;
				width = 235;
				height = 140;
				if (this._save.Upgrades.indexOf(contents[i].Name) > -1) {
					Render.UpgradeCard(left, top, contents[i], "#306060", "#80C0C0", true, true);
				} else if (contents[i].Requires !== undefined && this._save.Upgrades.indexOf(contents[i].Requires.Name) == -1) {
					Render.UpgradeCard(left, top, contents[i], "#303030", "#808080", false, true);
				} else {
					var canAfford = this._save.Funds >= contents[i].Cost;
					if (Utils.IsMouseOver(left, top, width, height)) {
						Render.UpgradeCard(left, top, contents[i], "#408080", "#90E0E0", false, canAfford);
						if (Input.Mouse.Clicked) {
							this._save.Funds -= current.Cost;
							this._save.Upgrades.push(contents[i].Name);
						}
					} else {
						Render.UpgradeCard(left, top, contents[i], "#306060", "#80C0C0", false, canAfford);
					}
				}
			}
		},
		Close: function() {
			Data.Set("saveData", JSON.stringify(this._save));
		}
	},
	Game: {
		_save: null,
		_resetPlayerPosition: function() {
			this._player.X = -80;
			this._player.Y = Render._canvas.height / 2;
		},
		_resetPlayerStats: function() {
			this._player.Speed = 250;				//pixels moved per second after upgrades and modifiers
			this._player.ProcessRate = 30;			//value processed per second
			this._player.ProcessCount = 1;			//number of simultaneous debris entities processed
			this._player.ProcessStrength = 200;		//amount of pull, in pixels per second, the ship processor exerts on debris being processed
			this._player.Range = 40;				//distance in pixels from the center of the ship you can catch debris to begin processing
			this._player.Angle = 60;				//angle of cone that can pick up debris in degrees
			this._player.Drones = 0;				//number of drones active
			this._player.DroneSpeed = 400;			//pixels drones move per second
			this._player.DroneRange = 2;			//time in seconds drones will actively collect before returning to the ship
			this._player.DroneMaxValue = 10;		//maximum value of debris a drone can process
			this._player.RayType = "none";			//type of ray attack, can be none, heat (decays debris), or cold (slows debris)
			this._player.RayRateOfFire = 1;			//number of ray pulses fired per second
			this._player.RayRange = 300;			//range of ray attacks in pixels
			this._player.RayPower = 25;				//power of ray attack, for heat this is value removed per 4 seconds, for cold this is rate of speed decay per second
			this._player.Processing.length = 0;
		},
		_player: {
			Processing: []
		},
		_elapsed: 0,
		_debris: [],
		_spawnMap: [],
		_endTime: null,
		_debrisMax: 10000,
		
		Open: function() {
			var saveData = null;
			try { saveData = JSON.parse(Data.Get("saveData")); } catch (e) { }
			if (saveData === null) {
				saveData = {
					Level: 0,
					Funds: 0,
					Debris: 0,
					Upgrades: [],
				};
				Data.Set("saveData", JSON.stringify(saveData));
			}
			this._save = saveData;
			this._resetPlayerPosition();
			this._resetPlayerStats();
			
			for (var property in Upgrades) {
				if (Upgrades.hasOwnProperty(property)) {
					if (this._save.Upgrades.indexOf(Upgrades[property].Name) >= 0) {
						Upgrades[property].Apply(this._player);
					}
				}
			}
			
			//generate level
			this._debris.length = 0;
			this._spawnMap.length = 0;
			var level = this._save.Level;
			var targetValue = (0.10 + level * 0.05) * (this._debrisMax * 2);
			var spawnRate = 1 / (3 + (level * 2));
			var baseValue = 10 + (level * 2);
			var range = 11 + (level * 2);
			var mockTime = 0;
			var currentValue = 0;
			while (currentValue <= targetValue) {
				var newValue = baseValue + Math.floor(Math.random() * (range * Math.log10(1 + (currentValue / targetValue) * 9)));
				this._spawnMap.push({
					Time: mockTime,
					Debris: {
						Value: newValue,
						X: 1000,
						Y: Math.floor(Math.random() * (Render._canvas.height - 40)) + 20,
						Ticks: 0,
						Processed: 0,
					},
				});
				mockTime += spawnRate;
				currentValue += newValue;
			}
			this._endTime = null;
			this._elapsed = 0;
		},
		
		Run: function(delta) {
			Render.Clear();
			this._elapsed += delta;
			if (this._elapsed < 5) {
				this._render(delta);
				this._runIntro(delta);
			} else if (this._spawnMap.length > 0 || this._debris.length > 0) {
				this._spawnDebris(delta);
				this._processDebris(delta);
				this._processPlayer(delta);
				this._render(delta);
			} else {
				if (this._endTime === null)
					this._endTime = this._elapsed;
				this._render(delta);
				this._runOutro(delta);
			}
		},
		
		Close: function() {
			this._save.Level++;
			Data.Set("saveData", JSON.stringify(this._save));
		},
		
		_runIntro: function(delta) {
			Utils.MoveTo(Render._canvas.width / 2, Render._canvas.height / 2, 140 * delta, this._player);
			if (this._elapsed < 1) {
				Render.Fade("#000000", 1 - this._elapsed);
			} else if (this._elapsed < 3) {
				var curElapsed = this._elapsed - 1;
				if (curElapsed < 0.2)
					Render.TextFade(Render._canvas.width / 2, Render._canvas.height / 2, "Ready...", 196, "#80C0C0", "#408080", Math.log10(1 + curElapsed / 0.2 * 9));
				else if (curElapsed < 1.8)
					Render.TextFade(Render._canvas.width / 2, Render._canvas.height / 2, "Ready...", 196, "#80C0C0", "#408080", 1);
				else
					Render.TextFade(Render._canvas.width / 2, Render._canvas.height / 2, "Ready...", 196, "#80C0C0", "#408080", Math.log10(1 + (0.2 - (curElapsed - 1.8)) / 0.2 * 9));
			} else if (this._elapsed < 5) {
				var curElapsed = this._elapsed - 3;
				if (curElapsed < 0.2)
					Render.TextFade(Render._canvas.width / 2, Render._canvas.height / 2, "GO!", 196, "#80C0C0", "#408080", Math.log10(1 + curElapsed / 0.2 * 9));
				else if (curElapsed < 1.8)
					Render.TextFade(Render._canvas.width / 2, Render._canvas.height / 2, "GO!", 196, "#80C0C0", "#408080", 1);
				else
					Render.TextFade(Render._canvas.width / 2, Render._canvas.height / 2, "GO!", 196, "#80C0C0", "#408080", Math.log10(1 + (0.2 - (curElapsed - 1.8)) / 0.2 * 9));
			}
		},
		
		_runOutro: function(delta) {
			var curElapsed = this._elapsed - this._endTime;
			if (curElapsed < 2) {
				Utils.MoveTo(Render._canvas.width / 2 - 200, Render._canvas.height / 2, this._player.Speed / 4 * delta, this._player);
			} else if (curElapsed < 3) {
				Utils.MoveTo(Render._canvas.width + 100, Render._canvas.height / 2, this._player.Speed * 2 * delta, this._player);
			} else if (curElapsed < 4) {
				Utils.MoveTo(Render._canvas.width + 100, Render._canvas.height / 2, this._player.Speed * 2 * delta, this._player);
				Render.Fade("#000000", (curElapsed - 3) / 1);
			} else {
				if (this._save.Debris >= this._debrisMax) {
					Scene.SwitchTo(Scenes.Defeat);
				} else if (this._save.Level >= 4) {
					Scene.SwitchTo(Scenes.Victory);
				} else {
					Scene.SwitchTo(Scenes.Upgrade);
				}
			}
		},
		
		_spawnDebris: function(delta) {
			var levelTime = this._elapsed - 5;
			while (this._spawnMap.length > 0 && this._spawnMap[0].Time < levelTime) {
				var current = this._spawnMap.shift();
				this._debris.push(current.Debris);
			}
		},
		
		_processPlayer: function(delta) {
			var pulse = Math.sin(this._elapsed * 60 * (Math.PI / 180));
			
			var speed = this._player.Speed;
			if (this._player.X < this._save.Debris / 10 - 20)
				speed /= 2;
			var targetX = Input.Mouse.X;
			var targetY = Input.Mouse.Y;
			if (Input.LastType == "keyboard") {
				targetX = this._player.X;
				targetY = this._player.Y;
				if (Input.IsKeyDown(37)) 
					targetX -= this._player.Speed * 2;
				if (Input.IsKeyDown(38)) 
					targetY -= this._player.Speed * 2;
				if (Input.IsKeyDown(39)) 
					targetX += this._player.Speed * 2;
				if (Input.IsKeyDown(40)) 
					targetY += this._player.Speed * 2;
			}
			Utils.MoveTo(targetX, targetY, speed * delta, this._player);
			this._player.X = Utils.Clamp(this._player.X, 0, Render._canvas.width);
			this._player.Y = Utils.Clamp(this._player.Y, 0, Render._canvas.height);
			var rangeSq = this._player.Range * this._player.Range;
			var halfAngle = this._player.Angle / 2;
			for (var i = 0; i < this._debris.length; i++) {
				if (this._player.Processing.length >= this._player.ProcessCount)
					break;
				var current = this._debris[i];
				var dx = current.X - this._player.X;
				var dy = current.Y - this._player.Y;
				if (dx * dx + dy * dy <= rangeSq) {
					var angle = (Math.atan2(dx, dy) * (180 / Math.PI) + 270) % 360;
					if (angle >= 360 - halfAngle || angle <= halfAngle) {
						this._player.Processing.push(current);
					}
				}
			}
			for (var i = 0; i < this._player.Processing.length; i++) {
				var current = this._player.Processing[i];
				Utils.MoveTo(this._player.X + 20, this._player.Y, this._player.ProcessStrength * delta, current);
				var dx = current.X - this._player.X;
				var dy = current.Y - this._player.Y;
				if (dx * dx + dy * dy > rangeSq) {
					current.Processed = 0;
					this._player.Processing.splice(i, 1);
				} else {
					var drain = Math.min(this._player.ProcessRate * delta, current.Value);
					current.Value -= drain;
					this._save.Funds += drain;
					current.Processed += delta;
					if (current.Value <= 0) {
						var elapsed = new Date().getTime();
						elapsed -= current._specialTime;
						this._player.Processing.splice(i, 1);
					}
				}
			}
		},
		
		_processDebris: function(delta) {
			for (var i = 0; i < this._debris.length; i++) {
				var current = this._debris[i];
				if (current.Value <= 0)
					continue;
				current.Ticks += delta;
				current.X -= 180 * delta;
				var endZone = Render._canvas.width * (this._save.Debris / this._debrisMax);
				if (current.X <  endZone - 20) {
					this._save.Debris += current.Value;
					current.Value = 0;
					if (this._save.Debris >= this._debrisMax) {
						this._spawnMap.length = 0;
					}
				}
			}
			for (var i = 0; i < this._debris.length; i++) {
				if (this._debris[i].Value <= 0) {
					this._debris.splice(i, 1);
				}
			}
		},
		
		_render: function(delta) {
			var scale = 2;
			var pulse = Math.sin(this._elapsed * 180 * (Math.PI / 180));

			for (var i = 0; i < this._debris.length; i++) {
				var current = this._debris[i];
				Render.Vector(current.X, current.Y, Graphics.DebrisSmall, scale * (current.Value / 10), current.Ticks * 180 + current.Processed * current.Processed * 720);
			}
			var endZone = Render._canvas.width * (this._save.Debris / this._debrisMax);
			Render.TrashLine(endZone, 20, 20, "#000000", "#404040");
			if (this._player.Range > 40 && this._player.Processing.length > 0)
				Render.Arc(this._player.X, this._player.Y, this._player.Range, this._player.Angle, "#a0ffff", 0.3);
			Render.Vector(this._player.X - 30, this._player.Y, Graphics.Player, 2, 0, 1 + pulse * 0.5);
		}
	}
};