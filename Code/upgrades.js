var Upgrades = {
	Initialize: function() {
		this.Thrusters1 = {
			Name: "Thrusters Mk II",
			Description: "An upgraded version of your ship's thrusters. Increases your ship's speed by 30%.",
			Cost: 500,
			Apply: function(player) {
				player.Speed *= 1.3;
			}
		};
		this.Thrusters2 = {
			Name: "Thrusters Mk III",
			Description: "A further improvement for your ship's thrusters. Increases your ship's speed by an additional 30%, up to ~70%.",
			Cost: 1500,
			Requires: Upgrades.Thrusters1,
			Apply: function(player) {
				player.Speed *= 1.3;
			}
		};
		this.Thrusters3 = {
			Name: "Thrusters Mk IV",
			Description: "Top-of-the-line thrusters for your ship. Increases your ship's speed by an additional 30%, up to ~120%.",
			Cost: 5000,
			Requires: Upgrades.Thrusters2,
			Apply: function(player) {
				player.Speed *= 1.3;
			}
		};
		this.ProcessRange1 = {
			Name: "Tractor Beam",
			Description: "Adds a tractor beam that automatically draws in nearby debris.",
			Cost: 1000,
			Apply: function(player) {
				player.Range += 30;
				player.Angle += 30;
			}
		};
		this.ProcessRange2 = {
			Name: "Tractor Field",
			Description: "Improves your tractor beam's range.",
			Cost: 2500,
			Requires: Upgrades.ProcessRange1,
			Apply: function(player) {
				player.Range += 30;
				player.Angle += 30;
			}
		};
		this.ProcessPower1 = {
			Name: "Deconstructor Mk II",
			Description: "Improved matter deconstructor that processes material 50% faster and is capable of holding larger debris.",
			Cost: 1000,
			Apply: function(player) {
				player.ProcessRate *= 1.5;
				player.ProcessStrength *= 1.5;
			}
		};
		this.ProcessPower2 = {
			Name: "Deconstructor Mk III",
			Description: "The ultimate in deconstructor technology, it processes material 50% faster than the Mk II, and can hold the heaviest debris.",
			Cost: 3000,
			Requires: Upgrades.ProcessPower1,
			Apply: function(player) {
				player.ProcessRate *= 1.5;
				player.ProcessStrength *= 1.5;
			}
		};
		this.ProcessCore = {
			Name: "Dual Deconstructor",
			Description: "Add a second core to your deconstructor, allowing you to process two objects at once.",
			Cost: 5000,
			Apply: function(player) {
				player.ProcessCount++;
			}
		};
	}
};
/*
Drones: 0,				//number of drones active
DroneBaseSpeed: 400,	//pixels drones move per second
DroneRange: 2,			//time in seconds drones will actively collect before returning to the ship
DroneMaxValue: 10,		//maximum value of debris a drone can process
RayType: "none",		//type of ray attack, can be none, heat (decays debris), or cold (slows debris)
RayRange: 300,			//range of ray attacks in pixels
RayPower: 25,			//power of ray attack, for heat this is value removed per 4 seconds, for cold this is rate of speed decay per second
*/

Upgrades.Initialize();

var UpgradeCategories = [{
		Name: "Thrusters",
		Contents: [Upgrades.Thrusters1, Upgrades.Thrusters2, Upgrades.Thrusters3]
	},{
		Name: "Processing",
		Contents: [Upgrades.ProcessRange1, Upgrades.ProcessRange2, Upgrades.ProcessPower1, Upgrades.ProcessPower2, Upgrades.ProcessCore]
	}];