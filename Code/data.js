var Data = {
	_enabled: false,
	
	Initialize: function() {
		this._enabled = (typeof(Storage) !== "undefined");
	},
	
	Get: function(key) {
		if (this._enabled) {
			return localStorage.getItem(key);
		}
		return null;
	},
	
	Set: function(key, value) {
		if (this._enabled) {
			localStorage.setItem(key, value);
			return value;
		}
		return null;
	},
	
	Remove: function(key) {
		if (this._enabled) {
			localStorage.removeItem(key);
		}
	},
	
	_createSeed: function() {
		return Math.floor(Math.random() * Number.MAX_SAFE_INTEGER);
	},
	
	Seed: function() {
		var seed = this.Get("seed");
		if (seed !== null)
			return seed;
		return this.Set("seed", this._createSeed());
	},
	
	RefreshSeed: function() {
		return this.Set("seed", this._createSeed());
	}
};