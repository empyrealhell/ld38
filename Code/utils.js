var Utils = {
	Clamp: function(value, min, max) {
		return Math.min(Math.max(value, min), max);
	},
	
	MoveTo: function(targetX, targetY, maxSpeed, obj) {
		var speed = maxSpeed;
		var xDist = targetX - obj.X;
		var yDist = targetY - obj.Y;
		var angle = Math.atan2(xDist, yDist);
		var totalDist = xDist * xDist + yDist * yDist;
		if (totalDist < speed * speed)
			totalDist = Math.sqrt(totalDist);
		else
			totalDist = speed;
		
		var xVel = Math.sin(angle) * totalDist;
		var yVel = Math.cos(angle) * totalDist;
		
		obj.X += xVel;
		obj.Y += yVel;
	},
	
	IsMouseOver: function(left, top, width, height) {
		return (Input.Mouse.X >= left && Input.Mouse.X <= left + width && Input.Mouse.Y >= top && Input.Mouse.Y <= top + height);
	},
	
	DtoR: function(degrees) {
		return degrees * (Math.PI / 180);
	},
	
	RtoD: function(radians) {
		return radians * (180 / Math.PI);
	},

	Random: {
		w: 32,
		n: 624,
		m: 397,
		r: 31,
		a: 0x9908B0DF16,
		u: 11,
		d: 0xFFFFFFFF16,
		s: 7,
		b: 0x9D2C568016,
		t: 15,
		c: 0xEFC6000016,
		l: 18,
		f: 1812433253,
		MT: [],
		_initialize: function() {
			this.index = this.n + 1;
			this.lower_mask = (1 << this.r) - 1;
			this.upper_mask =  ~this.lower_mask;
		},
		SetSeed: function(seed) {
			this.index = this.n;
			this.MT[0] = seed;
			for (var i = 1; i < this.n; i++)
				this.MT[i] = (this.f * (this.MT[i - 1] ^ (this.MT[i - 1] >> (this.w - 2))) + i);
		},
		Next: function() {
			if (this.index >= this.n) {
				if (this.index > this.n)
					this.SetSeed(new Date().getTime());
				this._twist();
			}
			var y = this.MT[this.index];
			y = y ^ ((y >> this.u) & this.d);
			y = y ^ ((y << this.s) & this.b);
			y = y ^ ((y << this.t) & this.c);
			y = y ^ (y >> this.l);
			this.index++;
			return y;
		},
		_twist: function() {
			for (var i = 0; i < this.n; i++) {
				var x = (this.MT[i] & this.upper_mask) + (this.MT[(i + 1) % this.n] & this.lower_mask);
				var xA = x >> 1;
				if ((x % 2) != 0)
					xA = xA ^ this.a;
				this.MT[i] = this.MT[(i + this.m) % this.n] ^ xA;
			}
			this.index = 0;
		}
	}
}
Utils.Random._initialize();