window.onload = function() {
	var canvas = document.getElementById("canvas");
	Render.Initialize(canvas);
	Input.Initialize(canvas);
	Data.Initialize();
	Scene.Initialize();
	
	Scene.SwitchTo(Scenes.Start);
}