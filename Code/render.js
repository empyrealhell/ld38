var Render = {
	_canvas: null,
	_context: null,
	
	Background: "#000000",
	
	Initialize: function(canvas) {
		this._canvas = canvas;
		this._context = this._canvas.getContext("2d");
		this._context.fillStyle = "#000000";
		this._context.font = "18px Calibri";
	},
	
	Clear: function() {
		this._context.fillStyle = this.Background;
		this._context.fillRect(0, 0, this._canvas.width, this._canvas.height);
	},
	
	Fade: function(color, alpha) {
		this._context.fillStyle = color;
		this._context.globalAlpha = alpha;
		this._context.fillRect(0, 0, this._canvas.width, this._canvas.height);
		this._context.globalAlpha = 1;
	},
	
	_gradientCache: [],
	GradientClear: function(x1, y1, x2, y2, color1, color2) {
		var gradient = null;
		for (var i = 0; i < this._gradientCache.length; i++) {
			var current = this._gradientCache[i];
			if (current.x1 == x1 && current.y1 == y1 && current.x2 == x2 && current.y2 == y2 && current.color1 == color1 && current.color2 == color2) {
				gradient = current.gradient;
				break;
			}
		}
		if (gradient === null) {
			gradient = this._context.createLinearGradient(x1 * this._canvas.width, y1 * this._canvas.height, x2 * this._canvas.width, y2 * this._canvas.height);
			gradient.addColorStop(0, color1);
			gradient.addColorStop(1, color2);
			this._gradientCache.push({
				x1: x1,
				y1: y1,
				x2: x2,
				y2: y2,
				color1: color1,
				color2: color2,
				gradient: gradient
			});
		}
		this._context.fillStyle = gradient;
		this._context.fillRect(0, 0, this._canvas.width, this._canvas.height);
	},
	
	Text: function(x, y, text, color) {
		this._context.font = "18px Calibri";
		this._context.textAlign = "start";
		this._context.textBaseline = "alphabetic";
		this._context.fillStyle = color;
		this._context.fillText(text, x, y);
	},
	
	CurrencyText: function(x, y, text, color, size) {
		this._context.font = size + "px Calibri";
		this._context.textAlign = "end";
		this._context.textBaseline = "alphabetic";
		this._context.fillStyle = color;
		this._context.fillText(text, x, y);
	},
	
	TextFade: function(x, y, text, size, color, outline, fade) {
		var scale = (1 - fade) + 1;
		this._context.setTransform(scale, 0, 0, scale, x, y);
		this._context.globalAlpha = fade;
		this._context.font = size + "px Calibri";
		this._context.textAlign = "center";
		this._context.textBaseline = "middle";
		this._context.strokeStyle = outline;
		this._context.lineWidth = size / 16;
		this._context.strokeText(text, 0, 0);
		this._context.fillStyle = color;
		this._context.fillText(text, 0, 0);
		this._context.setTransform(1, 0, 0, 1, 0, 0);
		this._context.globalAlpha = 1;
	},
	
	Title: function(x, y, text, color) {
		this._context.font = "150px Calibri";
		this._context.textAlign = "start";
		this._context.textBaseline = "alphabetic";
		this._context.fillStyle = color;
		this._context.fillText(text, x, y);
	},
	
	Line: function(x1, y1, x2, y2, color) {
		this.lineWidth = 1;
		this._context.beginPath();
		this._context.moveTo(x1, y1);
		this._context.lineTo(x2, y2);
		this._context.strokeStyle = color;
		this._context.stroke();
	},
	
	TrashLine: function(x, breaks, variance, outline, fill) {
		Utils.Random.SetSeed(breaks * 1000 + variance * 2000)
		this._context.strokeStyle = outline;
		this._context.lineWidth = 1;
		this._context.fillStyle = fill;
		this._context.moveTo(-10, this._canvas.height + 10);
		this._context.beginPath();
		this._context.lineTo(-10, -10);
		for (var i = 0; i <= breaks; i++) {
			var shift = Utils.Random.Next() % variance;
			this._context.lineTo(x - (variance / 2) + shift, (this._canvas.height + 20) / breaks * i);
		}
		this._context.lineTo(-10, this._canvas.height + 10);
		this._context.fill();
		this._context.stroke();
		for (var j = 0; j < x / 40 + 1; j++) {
			var linesToDraw = (Utils.Random.Next() % (breaks / 4)) + breaks / 4;
			var vertVariance = (this._canvas.height + 20) / (linesToDraw);
			for (var i = 0; i < linesToDraw; i++) {
				if (Utils.Random.Next() % 3 > 0) {
					var height1 = (Utils.Random.Next() % (vertVariance / 2));
					var height2 = height1 + ((Utils.Random.Next() % (vertVariance / 4)) + (vertVariance / 4 * 2));
					var height3 = height2 + ((Utils.Random.Next() % (vertVariance / 4)) + (vertVariance / 4 * 2));
					this._context.beginPath();
					var offset = Utils.Random.Next() % 40;
					this._context.moveTo(x - ((40 * j) + offset) - (Utils.Random.Next() % variance), vertVariance * i + height1);
					this._context.lineTo(x - ((40 * j) + offset) - (Utils.Random.Next() % variance), vertVariance * i + height2);
					this._context.lineTo(x - ((40 * j) + offset) - (Utils.Random.Next() % variance), vertVariance * i + height3);
					this._context.stroke();
				}
			}
		}
	},
	
	UpgradeCard: function(x, y, upgrade, baseColor, highlightColor, purchased, affordable) {
		var width = 235;
		var height = 140;
		this._context.fillStyle = highlightColor;
		this._context.fillRect(x, y, width, 26);
		this._context.fillStyle = baseColor;
		this._context.fillRect(x, y + 26, width, height - 26);
		this._context.strokeStyle = highlightColor;
		this._context.lineWidth = 1;
		this._context.strokeRect(x, y, width, height);
		this._context.font = "24px Calibri";
		this._context.textAlign = "start";
		this._context.textBaseline = "hanging";
		this._context.fillStyle = baseColor;
		this._context.fillText(upgrade.Name, x + 5, y + 5);
		this._context.font = "16px Calibri";
		this._context.fillStyle = highlightColor;
		this._context.textAlign = "end";
		if (!purchased) {
			if (!affordable) {
				this._context.fillStyle = "red";
			}
			this._context.fillText("¤" + upgrade.Cost, x + width - 5, y + 29);
		} else {
			this._context.fillText("Purchased!", x + width - 5, y + 29);
		}
		this._context.fillStyle = highlightColor;
		this._context.textAlign = "start";
		this._wrapText(x + 5, y + 47, width - 10, 18, upgrade.Description);
	},
	
	_wrapText: function(x, y, width, spacing, text) {
		var words = text.split(" ");
		var currentLine = words[0];
		var lastLine = currentLine;
		while (words.length > 0) {
			while (this._context.measureText(currentLine).width < width) {
				words.shift();
				lastLine = currentLine;
				if (words.length == 0)
					break;
				currentLine += " " + words[0];
			}
			this._context.fillText(lastLine, x, y);
			currentLine = words[0];
			y += spacing;
		}
	},
	
	Arc: function(x, y, distance, angle, color, opacity) {
		if (color === undefined)
			color = "#ffffff";
		if (opacity === undefined)
			opacity = 1;
		this._context.fillStyle = color;
		this._context.globalAlpha = opacity;
		this._context.beginPath();
		this._context.moveTo(x, y);
		this._context.arc(x, y, distance, Utils.DtoR(-angle / 2), Utils.DtoR(angle / 2));
		this._context.lineTo(x, y);
		this._context.fill();
		this._context.globalAlpha = 1;
	},
	
	Button: function(x, y, width, height, text, color, outline, textColor) {
		this._context.font = "36px Calibri";
		if (color != "none") {
			this._context.fillStyle = color;
			this._context.fillRect(x, y, width, height);
		}
		if (outline != "none") {
			this._context.strokeStyle = outline;
			this._context.lineWidth = 1;
			this._context.strokeRect(x, y, width, height);
		}
		this._context.textAlign = "center";
		this._context.textBaseline = "middle";
		this._context.fillStyle = textColor;
		this._context.fillText(text, x + width / 2, y + height / 2);
	},
	
	Vector: function(x, y, vector, scale, rotation, pulsate) {
		if (scale === undefined)
			scale = 1;
		if (rotation === undefined)
			rotation = 0;
		if (pulsate === undefined)
			pulsate = 1;
		this._context.setTransform(scale, 0, 0, scale, x, y);
		this._context.rotate(rotation * Math.PI / 180);
		for (var i = 0; i < vector.length; i++) {
			var current = vector[i];
			if (current.FillStyle != "none") {
				this._context.fillStyle = current.FillStyle;
				var opacity = current.Opacity;
				if (opacity < 1)
					opacity *= pulsate;
				this._context.globalAlpha = opacity;
				this._context.fill(current.Path);
				this._context.globalAlpha = 1;
			}
			if (current.StrokeWidth > 0 && current.StrokeStyle != "none") {
				this._context.strokeStyle = current.StrokeStyle;
				this._context.lineWidth = current.StrokeWidth;
				this._context.stroke(current.Path);
			}
		}
		this._context.setTransform(1, 0, 0, 1, 0, 0);
	}
};