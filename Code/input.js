var Input = {
	_rootElement: null,
	
	Initialize: function(root) {
		window.onkeydown = this._keyDown;
		window.onkeyup = this._keyUp;
		window.onmousemove = this._mouseMove;
		window.onmousedown = this._mouseDown;
		window.onmouseup = this._mouseUp;
		this._rootElement = root;
	},
	
	LastType: "none",
	
	IsKeyDown: function(key) {
		if (this._keys[key] !== undefined)
			return this._keys[key];
		return false;
	},
	_keys: [],
	_keyDown: function(event) {
		Input._keys[event.keyCode] = true;
		Input.LastType = "keyboard";
	},
	_keyUp: function(event) {
		Input._keys[event.keyCode] = false;
		Input.LastType = "keyboard";
	},
	
	Mouse: {
		X: 0,
		Y: 0,
		Clicked: false
	},
	
	_mouseMove: function(event) {
		var bounds = Input._rootElement.getBoundingClientRect();
		Input.Mouse.X = Math.floor(event.clientX - bounds.x);
		Input.Mouse.Y = Math.floor(event.clientY - bounds.y);
		Input.LastType = "mouse";
	},
	
	_mouseDown: function(event) {
		Input.Mouse.Clicked = true;
		Input.LastType = "mouse";
	},
	
	_mouseUp: function(event) {
		Input.Mouse.Clicked = false;
		Input.LastType = "mouse";
	}
};